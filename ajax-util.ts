class AjaxUtil{
    private xhr = new XMLHttpRequest();
    private baseUrl = 'http://localhost';
    send(conf):void {
        this.xhr.open(conf.method,this.baseUrl+conf.url);
        if(conf.reqtype){
            this.xhr.setRequestHeader('content-type',conf.reqtype);
        }
        this.xhr.onreadystatechange = function(){
            if(this.readyState==4){
                if(this.status==200){
                        conf.func(this.response);
                }
            }
        }
        this.xhr.send(conf.data);
    }
}
