class AjaxUtil {
    constructor() {
        this.xhr = new XMLHttpRequest();
        this.baseUrl = 'http://localhost';
    }
    send(conf) {
        this.xhr.open(conf.method, this.baseUrl + conf.url);
        if(conf.reqtype){
            this.xhr.setRequestHeader('content-type',conf.reqtype);
        }
        this.xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    conf.func(this.response);
                }
            }
        };
            this.xhr.send(conf.data);
    }
}
console.log('test');
